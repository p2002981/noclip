using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using cakeslice;

public class InteractHandling : MonoBehaviour
{
    public float reachDistance;
    public Transform pickableSlot;
    private Vector3 testingPos;

    private bool press;
    private List<Interactable> interactableArray;
    private Pickable pickedupObject;
    private Interactable inReach;

    private GameObject ui;

    // Start is called before the first frame update
    void Start()
    {
        testingPos = new Vector3(0, 1, 0) + transform.position + transform.forward * reachDistance;
        press = false;
        pickedupObject = null;
        inReach = null; 

        ui = GameObject.Find("Interaction");

    // Créer un tableau d'objet récupérable (plutôt que l'exécuté à chaque appel d'handlePickup)
        Object[] objs = GameObject.FindObjectsOfType(typeof(Interactable));
        interactableArray = new List<Interactable>();

        foreach(Object o in objs) {
            interactableArray.Add((Interactable)o);
        }

    }

    void showUI(bool activate) {
        if(activate) {
            ui.SetActive(true);// = true;
        }
        else {
            ui.SetActive(false);// = false;
        }
    }

    // Renvoie un Interactable atteignable
    // null si aucun n'est trouvé
    private Interactable findClosest(bool hide_pickable) {
        float minDistance = -1;
        float testedDistance;
        Interactable ret = null;
     
        foreach(Interactable p in interactableArray) {
            if(hide_pickable && p is Pickable)
                continue;

            testedDistance = Vector3.Distance(p.transform.position, testingPos);

            if(testedDistance <= reachDistance
                && (minDistance == -1 || testedDistance < minDistance)) {
                    minDistance = testedDistance;
                    p.GetComponent<Outline>().enabled = true;

                    if(ret != null)
                        ret.GetComponent<Outline>().enabled = false;
                    ret = p;
            }
            else {
                p.transform.GetComponent<Outline>().enabled = false;
            }

        }
        showUI(ret != null);
        return ret;
    }

// Pickable
    private void pickup() {
        pickedupObject = (Pickable)inReach;
        pickedupObject.GetComponent<Outline>().enabled = false;
        pickedupObject.createBound();
    }

    private void drop() {
        pickedupObject.removeBound();
        pickedupObject.transform.position = testingPos;
        pickedupObject = null;
    }

// Interactable
    private void give() {
        if(((Container)inReach).submitItem(pickedupObject)) {
            interactableArray.Remove(pickedupObject);
            drop();
        }
    }


// Interaction
    private void handleInteraction() {
        if(inReach is Openable)
            ((Openable) inReach).open();
        else if(pickedupObject == null && inReach is Pickable)
            pickup();
        else if(pickedupObject != null && inReach is Container)
            give();
        else if(pickedupObject != null)
            drop();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(!PauseMenu.isPaused()) {
            testingPos = new Vector3(0, 1, 0) + transform.position + transform.forward * reachDistance;
            if(pickedupObject == null)
                inReach = findClosest(false);
            else {
                inReach = findClosest(true);
                pickedupObject.transform.position = pickableSlot.position;
            }

            if(Input.GetAxis("Interact") != 0) {
                if(!press) {
                    handleInteraction();
                }
                press = true;
            }
            else
                press = false;
        }
    }

}
