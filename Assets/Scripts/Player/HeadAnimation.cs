using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadAnimation : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(!PauseMenu.isPaused()) {
            transform.LookAt(transform.position + GetComponentInParent<PlayerMovement>().currentDirection);
        }
    }
}
