using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IKFootSolver : MonoBehaviour
{
    [SerializeField] LayerMask terrainLayer = default;
    [SerializeField] Transform body = default;
    [SerializeField] IKFootSolver otherFoot = default;
    //[SerializeField] float speed = 1;
    private float speed;
    [SerializeField] float stepDistance = 4;
    [SerializeField] float stepLength = 4;
    [SerializeField] float stepHeight = 1;
    [SerializeField] Vector3 footOffset = default;
    float footSpacing;
    Vector3 oldPosition, currentPosition, newPosition;
    Vector3 oldNormal, currentNormal, newNormal;
    float lerp;

    Vector3 rayHit;

    private bool bForceFeetAlignement;
    public bool active = true;

    

    private void Start()
    {
        footSpacing = transform.localPosition.x;
        currentPosition = newPosition = oldPosition = transform.position;
        currentNormal = newNormal = oldNormal = transform.up;
        lerp = 1;
        speed = 1;

        bForceFeetAlignement = false;
    }

    // Update is called once per frame


    void FixedUpdate()
    {
        if(!PauseMenu.isPaused()) {
            speed = Mathf.Max(2.0f, 0.5f * body.transform.parent.parent.GetComponent<Rigidbody>().velocity.magnitude);
            transform.position = currentPosition;
            transform.up = currentNormal;

            Ray ray = new Ray(body.position + (body.right * footSpacing), Vector3.down);

            if (active && Physics.Raycast(ray, out RaycastHit info, 10, terrainLayer.value))
            {
                rayHit = info.point;
                if(bForceFeetAlignement && Vector3.Distance(currentPosition, rayHit) > stepDistance * 0.5f && !otherFoot.IsMoving() && lerp >= 1) {
                    lerp = 0;
                    newPosition = rayHit;
                    newNormal = info.normal;
                
                    bForceFeetAlignement = false;
                }
                else if (Vector3.Distance(newPosition, rayHit) > stepDistance && !otherFoot.IsMoving() && lerp >= 1)
                {
                    lerp = 0;
                    int direction = body.InverseTransformPoint(rayHit).z > body.InverseTransformPoint(newPosition).z ? 1 : -1;
                    newPosition = rayHit + (body.forward * stepLength * direction) + footOffset;
                    newNormal = info.normal;
                }
            }

            if (lerp < 1)
            {
                Vector3 tempPosition = Vector3.Lerp(oldPosition, newPosition, lerp);
                tempPosition.y += Mathf.Sin(lerp * Mathf.PI) * stepHeight;

                currentPosition = tempPosition;
                currentNormal = Vector3.Lerp(oldNormal, newNormal, lerp);
                lerp += Time.deltaTime * speed;
            }
            else
            {
                oldPosition = newPosition;
                oldNormal = newNormal;
            }
        }
    }

    private void OnDrawGizmos()
    {

        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(newPosition, stepDistance);
        Gizmos.color = Color.green;
        Gizmos.DrawSphere(currentPosition, 0.1f);
        Gizmos.color = Color.blue;
        Gizmos.DrawSphere(rayHit, 0.1f);
    }



    public bool IsMoving()
    {
        return lerp < 1;
    }

    public void forceFeetAlignement() {
        bForceFeetAlignement = true;
    }



}