using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    // Pour mettre en pause la physique quand le jeu est en pasue
    private bool gameHasPaused;
    private Vector3 pausedVelocity;
    private Vector3 pausedAngularVelocity;
    
    private float axisInput;

    public float playerAcceleration;
    public float maxSpeed;
    public float dashSpeedMultiplier;

    public IKFootSolver RightIKSolver;
    public IKFootSolver LeftIKSolver;


    public Vector3 currentDirection;

    private bool isFalling;
    private bool hasDashed;
    private bool isRunning;

    private float dashMultiplier;
    private float jumpMultiplier;

    private Transform cameraTransform;

    bool onlyOnce;
    bool inputReceived;

    void handleInput() {
        // Clavier OU Manette
        if((axisInput = Input.GetAxis("vKeyboard1")) != 0 || (axisInput = Input.GetAxis("vJoystick1")) != 0) {
            currentDirection += new Vector3(cameraTransform.forward.x, 0, cameraTransform.forward.z) * axisInput * dashMultiplier * jumpMultiplier;
            inputReceived = true;
        }
        if((axisInput = Input.GetAxis("hKeyboard1")) != 0 || (axisInput = Input.GetAxis("hJoystick1")) != 0) {
            currentDirection += new Vector3(cameraTransform.right.x, 0, cameraTransform.right.z) * axisInput * dashMultiplier * jumpMultiplier;
            inputReceived = true;
        }
        if(inputReceived && currentDirection.magnitude > 0.1f) {
        // On normalise la direction pour qu'elle "forme un cercle" autour du joueur plutôt qu'un carré
            currentDirection.Normalize();
        }

        if(Input.GetAxis("Jump") != 0 && !isFalling) {
            isFalling = true;
            GetComponent<Rigidbody>().AddForce(0, maxSpeed * 0.5f, 0, ForceMode.Impulse);
            
            RightIKSolver.active = false;
            LeftIKSolver.active = false;
        }
    }

    void handleState() {
    // Gestion chute
        if(!isFalling){
            // Réinitialisation du vecteur direction
            currentDirection = new Vector3(0, 0, 0);
            currentDirection = new Vector3(0, 0, 0);
        }
        else {
            jumpMultiplier = 0.01f;
            currentDirection *= 0.99f;
        }

    // Gestion Dash
        if(!isFalling && !hasDashed && Input.GetAxis("Dash") != 0) {
            isRunning = true;
            hasDashed = true;
            dashMultiplier = dashSpeedMultiplier * 4.0f;
        }
        if(isRunning) {
            dashMultiplier -= dashSpeedMultiplier;
            dashMultiplier *= 0.8f;
            dashMultiplier += dashSpeedMultiplier;
        }
        if(dashMultiplier < dashSpeedMultiplier + 0.00001f) // Pour pouvoir re-dasher après un certain temps
            hasDashed = false;
        if(GetComponent<Rigidbody>().velocity.magnitude < maxSpeed * 0.5f) {
            isRunning = false;
            hasDashed = false;
            dashMultiplier = 1.0f;
        }

    // Gestion arrêt
        if(!inputReceived) {
            if(!onlyOnce) {
                RightIKSolver.forceFeetAlignement();
                LeftIKSolver.forceFeetAlignement();
                onlyOnce = true;
            }
        }
        else {
            if(GetComponent<Rigidbody>().velocity.magnitude > 0.1f) {
                // Fait tourner le personnage dans la direction de déplacement
                Vector3 lookDir = transform.position + GetComponent<Rigidbody>().velocity;
                transform.LookAt(new Vector3(lookDir.x, transform.position.y, lookDir.z));
            }
            onlyOnce = false;

            inputReceived = false;
        }
    }

    // Calcule le vecteur vitesse en fonction du vecteur direction
    void ComputeVelocity() {
        Vector3 addedVelocity = playerAcceleration * (maxSpeed * currentDirection - Vector3.Scale(GetComponent<Rigidbody>().velocity, new Vector3(1, 0, 1)));

        GetComponent<Rigidbody>().velocity += addedVelocity;
            
    }

    // Start is called before the first frame update
    void Start()
    {
        // Récupère le Trasnform de la caméra
        cameraTransform = transform.GetChild(0).transform;
        currentDirection = new Vector3(0, 0, 0);
        currentDirection = transform.forward;
        dashMultiplier = 1.0f;
        jumpMultiplier = 1.0f;
        isRunning = false;
        isFalling = false;
        hasDashed = false;

        onlyOnce = false;
        inputReceived = false;

        gameHasPaused = false;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(!PauseMenu.isPaused()) {
            if(gameHasPaused) {
                GetComponent<Rigidbody>().velocity = pausedVelocity;
                GetComponent<Rigidbody>().angularVelocity = pausedAngularVelocity;
                GetComponent<Rigidbody>().isKinematic = false;
                gameHasPaused = false;
            }
            
            handleState();
            handleInput();
            ComputeVelocity();
            
        }
        else {
            if(!gameHasPaused) {
                pausedVelocity = GetComponent<Rigidbody>().velocity;
                pausedAngularVelocity = GetComponent<Rigidbody>().angularVelocity;
                GetComponent<Rigidbody>().isKinematic = true;
                gameHasPaused = true;
            }
        }
    }

    void OnCollisionEnter() {
        isFalling = false;
        jumpMultiplier = 1.0f;

        
        RightIKSolver.active = true;
        LeftIKSolver.active = true;
    }
}
