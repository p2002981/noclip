using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FeetAnimation : MonoBehaviour
{

     private Transform footBase;
     private Vector3 lookpos;
     private Transform robot;

    // Start is called before the first frame update
    void Start()
    {
        lookpos = new Vector3(0, 0, 0);
        footBase = transform.GetChild(0);
        robot = GameObject.Find("Hips").transform;
    }

    // Update is called once per frame
    void Update()
    {
        if(!PauseMenu.isPaused()) {
            if(Physics.Raycast(footBase.position, Vector3.down, out RaycastHit hitInfo, 10.0f)) {
                lookpos = transform.position + Vector3.Cross(robot.right, hitInfo.normal);
                transform.LookAt(lookpos);
            }
        }
    }

    
    public void OnDrawGizmos() {
        Gizmos.color = Color.blue;
        Gizmos.DrawSphere(lookpos, 0.1f);
    }
    
}
