using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    private Vector2 cam_angle;
    private Vector3 player_pos;
    public float cam_speed;
    public float cam_distance;

    private float vInput;
    private float hInput;
    

    // Calcul la nouvelle position de la caméra en fonction de l'angle
    // & Fait tourner la caméra pour qu'elle suive le joueur
    void moveCam() {
        player_pos = transform.parent.position + new Vector3(0, 1, 0);
        transform.position = player_pos + cam_distance * new Vector3(
                Mathf.Sin(cam_angle.y) * Mathf.Cos(cam_angle.x),
                Mathf.Cos(cam_angle.y),
                Mathf.Sin(cam_angle.y) * Mathf.Sin(cam_angle.x)
            );
        // Fait tourner la caméra sur elle même
        transform.LookAt(player_pos);
        return;
    }

    void rectifyForCollision() {
        Vector3 rayDir = Vector3.Normalize(transform.position - player_pos);
        if(Physics.Raycast(
            player_pos, 
            rayDir,
            out RaycastHit rayHit,
            cam_distance - 1.0f
        )) {
            transform.position = rayHit.point - rayDir;
        }
    }

    // Gère les input utilisateurs pour déplacer la caméra
    // Renvoie vrai si la caméra a été déplacée
    void handleInput() {
        // Récupère le déplacement de la caméra
        Vector2 mouseInput = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));
        
        // Met à jour l'angle de la caméra
        cam_angle += new Vector2(
            -mouseInput.x * cam_speed,
            -mouseInput.y * cam_speed
        );

        if((vInput = Input.GetAxis("vJoystick2")) != 0)
            cam_angle.y -= vInput * 0.05f;
        if((hInput = Input.GetAxis("hJoystick2")) != 0)
            cam_angle.x -= hInput * 0.05f;

        // Bloque la caméra verticalement
        if(cam_angle.y >= -Mathf.PI / 4)
            cam_angle.y = -Mathf.PI / 4;
        else if(cam_angle.y <= -Mathf.PI * 3 / 4)
            cam_angle.y = -Mathf.PI * 3 / 4;
    }

    void Start()
    {
        player_pos = transform.parent.position + new Vector3(0, 1, 0);
        cam_angle = new Vector2(0, -Mathf.PI * 0.25f);
        moveCam();
    }

    void FixedUpdate()
    {
        if(!PauseMenu.isPaused()) {
            handleInput();
            moveCam();
            rectifyForCollision();
        }
    }
}
