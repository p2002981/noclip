using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UiPc : MonoBehaviour
{

    private RawImage logo;
    public Texture[] textureLogo=new Texture[3];
    private TextMeshProUGUI processText;
    private Image lvlMask;

    // Start is called before the first frame update
    public void init(Computer currentPC)
    {
        logo=GameObject.Find("Logo").GetComponent<RawImage>();
        logo.gameObject.SetActive(false);
        
        processText = GameObject.Find("Texte").GetComponent<TextMeshProUGUI>();
        processText.text="Veuillez insérer une disquette";
        
        lvlMask= GameObject.Find("mask").GetComponent<Image>();
        lvlMask.fillAmount=0f;
    }

    public void choixImage(Computer.Type_os OS){
        logo.gameObject.SetActive(true);
        switch (OS){
            case Computer.Type_os.MACOS:
                logo.texture=textureLogo[0];
            break;
            case Computer.Type_os.WINDOWS:
                logo.texture=textureLogo[1];
            break;
            case Computer.Type_os.LINUX:
                logo.texture=textureLogo[2];
            break;
        }
        
    }

    public void progressBarChange(Computer pc){
        lvlMask.fillAmount = pc.getProgressPC()/100f;
        if(pc.getProgressPC() < 100f)
            processText.text="Il vous reste " + pc.getTotalDiskette() * (1f - pc.getProgressPC() / 100f) + " disquettes à installer";
        else
            processText.text="L'OS a été installé !";
    }
}
