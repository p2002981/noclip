using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    private static bool gamePause;
    private GameObject pauseMenu;
    private bool keyPressed;

    public void Start() {
        pauseMenu = GameObject.Find("Pause Menu");
        pauseMenu.SetActive(false);
        keyPressed = false;
    }


    private void setPause(bool paused) {
        pauseMenu.SetActive(paused);
        PauseMenu.gamePause = paused;
    }

    public static bool isPaused() {
        return PauseMenu.gamePause;
    }



    public void FixedUpdate() {
        if(Input.GetAxis("Menu") != 0.0f) {
            if(!keyPressed) {
                setPause(!PauseMenu.gamePause);
                keyPressed = true;
            }
        }
        else 
            keyPressed = false;
    }



    public void returnGame() {
        setPause(false);
    }

    public void retryGame() {
        SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene().name);
        SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().name);
        setPause(false);
    }

    public void exitGame() {
        Application.Quit();
    }
}

