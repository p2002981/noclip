using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public abstract class Container : Interactable
{
    public abstract bool submitItem(Pickable item);
}