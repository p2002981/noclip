using System.Collections;
using System.Collections.Generic;
using UnityEngine;

 


public class Computer : Container
{
    public enum Type_os {MACOS, WINDOWS, LINUX, NONE};

    private float install_progress;
    private Type_os installed_os;
    private UiPc UIPc;

    
    protected override void init()
    {
        installed_os=Type_os.NONE;
        install_progress=0f;
        UIPc= GameObject.Find("UiPC").GetComponent<UiPc>();
        UIPc.init(this);
    }

    public override bool submitItem(Pickable item) {
        if(item is Diskette)
            if( insertDisk(((Diskette)item).OS) ) {
                item.gameObject.SetActive(false);
                return true;
            }
        return false;
    }

    public bool insertDisk(Type_os os){
        if(installed_os == Type_os.NONE){
            installed_os = os;
            UIPc.choixImage(os);
        }
        if(installed_os == os) {
            install_progress += 25.0f;
            UIPc.progressBarChange(this);
            Debug.Log("Progrès actuel: " + install_progress);
        }
        else {
            Debug.Log("Diskette refusée");
            return false;
        }
        return true;
    }

    public Type_os getOS(){
        return installed_os;
    }

    public float getProgressPC(){
        return install_progress;
    }

    public int getTotalDiskette() {
        return 4;
    }
}