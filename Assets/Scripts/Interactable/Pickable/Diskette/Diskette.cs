using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Diskette : Pickable {
    protected Computer computer;
    public Computer.Type_os OS;
    private static int nbrLinux;
    private static int nbrMacOs;
    private static int nbrWindows;
    private static int nbrDisk;

    protected override void init() {
        nbrLinux = 0;
        nbrMacOs = 0;
        nbrWindows = 0;
        nbrDisk = 3;
        
        MeshRenderer mesh= GetComponent<MeshRenderer>();
        bool choixOsFait=false;
        while(!choixOsFait){
            switch (Random.Range(0,4)){
                case 0:
                    if(nbrLinux<4){
                        OS=Computer.Type_os.LINUX;
                        mesh.material.color= Color.black;
                        nbrLinux++;
                        choixOsFait=true;
                        Debug.Log("Disk type Linux n°" +nbrLinux+".");
                    }
                break;
                case 1:
                    if(nbrMacOs<4){
                        OS=Computer.Type_os.MACOS;
                        mesh.material.color= Color.gray;
                        nbrMacOs++;
                        choixOsFait=true;
                        Debug.Log("Disk type MacOs n°" +nbrMacOs+".");
                    }
                break;
                case 2:
                    if(nbrWindows<4){
                        OS=Computer.Type_os.WINDOWS;
                        mesh.material.color= Color.cyan;
                        nbrWindows++;
                        choixOsFait=true;
                        Debug.Log("Disk type Windows n°" +nbrWindows+".");
                    }
                break;
                case 3:
                    if(nbrDisk>0){
                        nbrDisk--;
                        choixOsFait=true;
                        gameObject.SetActive(false);
                    }
                break;
            }
        }
    }

    protected override void boundBehaviour() {
        return;
    }

    protected override void unboundBehaviour() {
        transform.position -= new Vector3(0, 2, 0);
    }
}