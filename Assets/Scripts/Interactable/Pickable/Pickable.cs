using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public abstract class Pickable : Interactable
{

    protected abstract void boundBehaviour();
    public void createBound() {
        
        if((GetComponent<Rigidbody>()) != null)
            GetComponent<Rigidbody>().useGravity = false;

        boundBehaviour();

    }

    protected abstract void unboundBehaviour();
    public void removeBound() {
        Debug.Log("Unbound " + transform.name);

        unboundBehaviour();

        if((GetComponent<Rigidbody>()) != null)
            GetComponent<Rigidbody>().useGravity = true;

        
    }
}
