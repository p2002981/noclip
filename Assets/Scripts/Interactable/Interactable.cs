using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using cakeslice;

public abstract class Interactable : MonoBehaviour {
    Outline outline;

    public void Start() {
        outline = gameObject.AddComponent(typeof(Outline)) as Outline;
        transform.GetComponent<Outline>().enabled = false;
        init();
    }

    abstract protected void init();
}