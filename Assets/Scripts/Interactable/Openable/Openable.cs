using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using cakeslice;

public class Openable : Interactable {
    private Transform gond;
    private bool isOpen;
    private float openingAngle;
    private float timePassed;
    private float dir;

    protected override void init() {
        gond = transform.parent;
        isOpen = false;
        openingAngle = 0f;
        dir = 1f;
    }

    public void FixedUpdate() {
        if(isOpen && openingAngle >= 90) return;
        if(!isOpen && openingAngle <= 0) return;

        float rotation;
        
        if(isOpen)
            rotation = dir * Mathf.Min(90f - openingAngle, rotationFunction());
        else
            rotation = dir * Mathf.Min(openingAngle, rotationFunction());

        transform.RotateAround(gond.position, transform.up, rotation);
        openingAngle += rotation;

        timePassed++;
    }

    private float rotationFunction() {
        return (1f/90f) * timePassed * timePassed + timePassed;
    }

    public void open() {
        isOpen = !isOpen;
        dir = isOpen ? 1f : -1f;
        timePassed = 0;
    }
}