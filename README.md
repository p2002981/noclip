# Animation Procédurale (AM2)
>Par BOMARD Stéphane  
Et BERTHOLON Noah

## Objectif
Le but de ce projet était de créer un jeu mettant en avant l'animation procédurale d'un personnage.  

Le but du jeu est d'installer un OS sur un ordinateur en récupérant des disquettes disséminées dans un appartement. Il existe trois type de disquette: MacOS (Grises), Windows (Cyans) et GNU/Linux (Noires). Une fois l'installation d'un OS initée, il n'est plus possible d'insérer des disquettes d'un autre OS. Ainsi il faut trouver 4 disquettes de la couleur qu'on aura choisi.

## Installation
### Version Unity : 2021.3.10f1
1. Exécuter `git clone git@forge.univ-lyon1.fr:p2002981/noclip.git`
2. Récupérer le fichier `model` ([lien](https://www.grosfichiers.com/SnVDUph8mBK) : expiration le 30 décembre)
3. Décompresser l'archive dans `noclip/Assets/.`
4. Ouvrir `noclip` en tant que projet sous Unity
5. Ouvrir la scène: `Scenes > Jeu`  

- Pour lancer la démo, cliquer sur le bouton <kbd>play</kbd> au dessus de la scène.  
- Pour créer un exécutable, dans la barre de menu: `File > Build and run`

## Contrôle
Nous conseillons d'uiliser une manette pour une expérience plus agréable.

Fonction | Manette | Clavier
-|-|-
Déplacer personnage | <kbd>Joystick gauche</kbd> | <kbd>z</kbd> <kbd>q</kbd> <kbd>s</kbd> <kbd>d</kbd>
Déplacer caméra | <kbd>Joystick droit</kbd> | <kbd>Souris</kbd>
Sauter | <kbd>A</kbd> | <kbd>Espace</kbd>
Intéragir | <kbd>X</kbd> | <kbd>x</kbd>
Ouvrir le menu | <kbd>Option / ≡</kbd> | <kbd>Échap</kbd>
Valider option menu | <kbd>X</kbd> | <kbd>Clique gauche</kbd>
(envoyé par mail)

## Résultats attendus
Le joueur doit être capable de contrôler le robot (déplacement & saut), récupérer des disquettes, les insérer dans l'ordinateur, et ouvrir des portes.

Les jambes du robots doivent être animées, et revenir sous le robot quand il se met à l'arrêt. Sa tête doit suivre la direction dans laquel il se dirige.

L'ordinateur doit indiquer la progression de l'installation, ainsi qu'afficher quel OS est en train d'être installé.
Il doit avoir 4 disquettes pour chaque OS.

Les portes doivent pouvoir s'ouvrir à un angle de 90° avec une animation progressive.

Les objets avec lesquels on peut intéragir doivent avoir un contour en surbrillance lorsqu'on peut les récupérer.

# Explication du code
## Hiérarchie
```
Assets
├── logo                    : Image pour l'UI de l'ordinateur
├── Materials               : Couleur pour les éléments créés à la main
├── model                   : Textures & Modèles récupérés sur internet
├── Scenes                  : Les différentes scènes sous Unity
└── Scripts                 : Tous les scripts
    ├── Interactable        : Scripts des objets avec lesquels on peut intéragir
    │   ├── Container       : Scripts des objets pouvant recevoir des Pickable
    │   ├── Openable        : Scripts des objets ouvrable (porte)
    │   └── Pickable        : Scripts des objets pouvant être récupéré par le personnage
    │       └── Diskette    : Scripts des disquettes
    ├── OutlineEffect       : Scripts pour les contours en surbrillance récupéré sur internet
    ├── Player              : Scripts relatifs au robot
    └── UI                  : Scripts des UI
```

## Scripts réutilisés
Script de Cinématique Inverse : Unity [source](https://docs.unity3d.com/Packages/com.unity.animation.rigging@1.0/manual/constraints/TwoBoneIKConstraint.html)  
Script original pour calculer la position du pied : Unity [source](https://www.youtube.com/watch?v=acMK93A-FSY)  
Script de contour en surbrillance : [source](https://github.com/cakeslice/Outline-Effect)
